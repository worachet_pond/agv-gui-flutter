import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class AgvServices {
  static String url = "http://192.168.45.117:3000";

  static Map<String, String> headers = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
  };
  static Future commandPut(String command) async {
    final response = await http
        .put(Uri.parse(url + '/command?command=' + command), headers: headers);
    if (response.statusCode != 200) {
      print(response.statusCode);
    }
  }

  static Future programPost(dynamic jobList) async {
    var body = json.encode({"program": jobList});
    final response = await http.post(Uri.parse(url + '/program'),
        headers: headers, body: body);
    if (response.statusCode != 200) {
      print(response.statusCode);
    }
  }

  static Future programSave(ProgramData program) async {
    final response = await http.post(Uri.parse(url + '/program/save'),
        headers: headers, body: json.encode(program.toJson()));
    if (response.statusCode != 200) {
      print(response.statusCode);
    }
  }

  static Future pathPost(dynamic pointList, String name) async {
    var body = json.encode({"name": name, "path": pointList});

    final response =
        await http.post(Uri.parse(url + '/path'), body: body, headers: headers);
    if (response.statusCode != 200) {
      print(response.statusCode);
    }
  }

  static Future<List<PathData>> pathGet() async {
    final response = await http.get(url + "/path", headers: headers);
    List<dynamic> map = json.decode(response.body);
    List<PathData> pathResponse = [];
    for (Map data in map) {
      pathResponse.add(PathData.fromJson(data));
    }
    return pathResponse;
  }

  static Future<List<ProgramData>> getProgram() async {
    final response = await http.get(url + "/program", headers: headers);
    List<dynamic> map = json.decode(response.body);
    List<ProgramData> programResponse = [];
    for (Map data in map) {
      programResponse.add(ProgramData.fromJson(data));
      // print(ProgramData(data["destination"], data["path"]).destination);
    }
    // print(programResponse.length);
    return programResponse;
  }

  static Future<double> getBattery() async {
    final response = await http.get(url + "/battery");
    Map map = json.decode(response.body);
    return map["battery"];
  }

  static Future deletePath(String name) async {
    final response =
        await http.delete(url + "/path?name=" + name, headers: headers);
    if (response.statusCode != 200) {
      print(response.statusCode);
    }
  }

  static Future<PointCoordinate> getPoint() async {
    final response = await http.get(url + "/pose");
    PointCoordinate res = PointCoordinate.fromJson(json.decode(response.body));
    return res;
  }

  static bool jogReady = true;
  static void putJog(String theta, String r, String speed) async {
    jogReady = false;
    var body = json.encode({"theta": theta, "r": r, "speed": speed});
    final response =
        await http.put(url + '/agv/jog', body: body, headers: headers);
    if (response.statusCode == 200) {
      jogReady = true;
    }
  }
}

class ProgramData {
  String destination;
  List path;
  ProgramData(this.destination, this.path);
  ProgramData.fromJson(Map<String, dynamic> json)
      : destination = json["destination"],
        path = json["path"];

  Map<String, dynamic> toJson() => {"destination": destination, "path": path};
}

class PointCoordinate {
  double x;
  double y;
  PointCoordinate(this.x, this.y);
  PointCoordinate.fromJson(Map<String, dynamic> json) {
    x = json["x"];
    y = json["y"];
  }
}

class PathData {
  String name;
  List path;
  PathData(String _name, dynamic _path);
  PathData.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    path = json["point"];
  }
}

class PointData {
  int id;
  double x;
  double y;
  double speed;
  double accel;
  PointData(this.id, this.x, this.y, this.speed, this.accel);
  PointData.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        x = json["x"],
        y = json["y"],
        speed = json["speed"],
        accel = json["accel"];
  Map<String, dynamic> toJson() =>
      {'id': id, 'x': x, 'y': y, 'speed': speed, 'accel': accel};
}

class TaskData {
  String name;
  Icon icon;
  Color color;
  TaskData(String _name, Icon _icon, Color _color) {
    name = _name;
    icon = _icon;
    color = _color;
  }
  Map<String, dynamic> toJson() => {'name': name};
}
