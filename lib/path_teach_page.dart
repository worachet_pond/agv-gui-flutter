import 'dart:async';

import 'package:flutter/material.dart';
import 'package:control_pad/control_pad.dart';
import 'package:agv_gui/api.dart';

class JobTeach extends StatefulWidget {
  @override
  _JobTeachState createState() => _JobTeachState();
}

class _JobTeachState extends State<JobTeach> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Path Teaching", style: TextStyle(color: Colors.white)),
      ),
      // backgroundColor: Colors.grey[800],
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: <Widget>[
              Coordinate(),
              SpeedSeting(),
              AccelSetting(),
              SizedBox(height: 10),
              ButtonBar(
                alignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                      padding: EdgeInsets.all(20),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Text("ADD POINT"),
                      color: Colors.green[200],
                      hoverColor: Colors.lightGreenAccent,
                      onPressed: () {
                        setState(() {
                          if (DataCollection.pointList.length > 0) {
                            DataCollection.pointList.add(PointData(
                                DataCollection.pointList.last.id + 1,
                                DataCollection.coordinateX,
                                DataCollection.coordinateY,
                                DataCollection.speedValue,
                                DataCollection.accelValue));
                          } else {
                            DataCollection.pointList.add(PointData(
                                0,
                                DataCollection.coordinateX,
                                DataCollection.coordinateY,
                                DataCollection.speedValue,
                                DataCollection.accelValue));
                          }
                        });
                      })
                ],
              ),
              Spacer(),
              JogBuild(),
            ],
          ),
          PointListBuild(),
          Spacer(),
          Column(
            children: [
              Spacer(),
              ButtonBar(
                children: [
                  RaisedButton(
                    onPressed: () {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                  title: Text("Path detail"),
                                  content: PathDetail(),
                                  actions: <Widget>[
                                    FlatButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'Cancel'),
                                        child: Text('Cancel')),
                                    FlatButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: Text('OK')),
                                  ])).then((value) {
                        if (value == "OK") {
                          String pathName = DataCollection.pathOrigin +
                              "_" +
                              DataCollection.pathDestination;
                          AgvServices.pathPost(
                              DataCollection.pointList, pathName);
                          // print(pointListJson);
                          DataCollection.pointList.clear();
                          Navigator.of(context).pop();
                        }
                      });
                    },
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(fontSize: 20),
                    ),
                    color: Colors.blue,
                    padding: EdgeInsets.all(20),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}

class Coordinate extends StatefulWidget {
  @override
  _CoordinateState createState() => _CoordinateState();
}

class _CoordinateState extends State<Coordinate> {
  bool isGetCoordinate = false;
  bool _validFieldX = true;
  bool _validFieldY = true;
  final coordinateControllerX = TextEditingController();
  final coordinateControllerY = TextEditingController();
  final _getCoordinateStream = StreamController<PointCoordinate>();
  Timer _timerGetCoordinate;
  @override
  void initState() {
    super.initState();
    void showCoordinateWidget(PointCoordinate point) {
      DataCollection.coordinateX = point.x;
      DataCollection.coordinateY = point.y;
      coordinateControllerX.text = point.x.toStringAsFixed(3);
      coordinateControllerY.text = point.y.toStringAsFixed(3);
    }

    _getCoordinateStream.stream.listen(showCoordinateWidget);
  }

  void dispose() {
    _timerGetCoordinate?.cancel();
    _getCoordinateStream.close();
    // Clean up the controller when the widget is disposed.
    coordinateControllerX.dispose();
    coordinateControllerY.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.lightGreen[100],
            borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              "Coordinate",
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
              ),
            ),
            Row(
              children: [
                Text(
                  'Read coordinate',
                  style: TextStyle(color: Colors.black),
                ),
                Switch(
                    value: isGetCoordinate,
                    onChanged: (bool value) {
                      setState(() {
                        this.isGetCoordinate = value;
                        if (this.isGetCoordinate) {
                          _timerGetCoordinate = Timer.periodic(
                              Duration(milliseconds: 500), (time) {
                            AgvServices.getPoint().then((value) {
                              this
                                  ._getCoordinateStream
                                  .sink
                                  .add(PointCoordinate(value.x, value.y));
                            });
                          });
                        } else {
                          _timerGetCoordinate?.cancel();
                        }
                      });
                    }),
              ],
            ),
            Row(
              children: [
                SizedBox(
                    width: 100,
                    child: TextField(
                      enabled: !this.isGetCoordinate,
                      controller: coordinateControllerX,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          labelText: "X :",
                          hintText: "X(meter)",
                          hintStyle: TextStyle(color: Colors.grey),
                          errorText: _validFieldX ? null : 'Decimal only'),
                      keyboardType: TextInputType.numberWithOptions(
                          signed: true, decimal: true),
                      onSubmitted: (String val) {
                        if (_validFieldX) {
                          DataCollection.coordinateX = double.parse(val);
                        }
                      },
                      onChanged: (String val) {
                        // AgvServices.getTime();
                        final decVal = double.tryParse(val);
                        if (decVal == null) {
                          setState(() => _validFieldX = false);
                        } else {
                          setState(() => _validFieldX = true);
                          if (_validFieldX) {
                            DataCollection.coordinateX = double.parse(val);
                          }
                        }
                      },
                    )),
                SizedBox(
                    width: 100,
                    child: TextField(
                      enabled: !this.isGetCoordinate,
                      controller: coordinateControllerY,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          labelText: "Y :",
                          hintText: "Y(meter)",
                          hintStyle: TextStyle(color: Colors.grey),
                          errorText: _validFieldY ? null : 'Decimal only'),
                      keyboardType: TextInputType.numberWithOptions(
                          signed: true, decimal: true),
                      onSubmitted: (String val) {
                        if (_validFieldX) {
                          DataCollection.coordinateY = double.parse(val);
                        }
                      },
                      onChanged: (String val) {
                        final decVal = double.tryParse(val);
                        if (decVal == null) {
                          setState(() => _validFieldY = false);
                        } else {
                          setState(() => _validFieldY = true);
                          if (_validFieldX) {
                            DataCollection.coordinateY = double.parse(val);
                          }
                        }
                      },
                    )),
              ],
            ),
          ],
        ));
  }
}

class SpeedSeting extends StatefulWidget {
  @override
  _SpeedSetingState createState() => _SpeedSetingState();
}

class _SpeedSetingState extends State<SpeedSeting> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
          color: Colors.lightBlue[100],
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Text(
            "speed",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          Slider(
            value: DataCollection.speedValue,
            min: 0.1,
            max: 1.0,
            divisions: 90,
            label: DataCollection.speedValue.toStringAsPrecision(2),
            onChanged: (double value) {
              setState(() {
                DataCollection.speedValue = value;
              });
            },
          ),
        ],
      ),
    );
  }
}

class AccelSetting extends StatefulWidget {
  @override
  _AccelSettingState createState() => _AccelSettingState();
}

class _AccelSettingState extends State<AccelSetting> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
          color: Colors.amber[100], borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Text(
            "Accel/Decel",
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          Slider(
            value: DataCollection.accelValue,
            min: 0.05,
            max: 0.5,
            divisions: 45,
            label: DataCollection.accelValue.toStringAsPrecision(2),
            onChanged: (double value) {
              setState(() {
                DataCollection.accelValue = value;
              });
            },
          ),
        ],
      ),
    );
  }
}

class PointListBuild extends StatefulWidget {
  @override
  _PointListBuildState createState() => _PointListBuildState();
}

class _PointListBuildState extends State<PointListBuild> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.deepPurple[100],
          borderRadius: BorderRadius.circular(10)),
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        // mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
                // color: Colors.lightBlueAccent[100],
                borderRadius: BorderRadius.circular(10)),
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              "Point list",
              style: TextStyle(fontSize: 30.0),
            ),
          ),
          SizedBox(
            width: 300,
            height: 500,
            child: ListView.builder(
                itemCount: DataCollection.pointList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text('$index'))),
                    title: Text(
                        "X : ${DataCollection.pointList[index].x.toStringAsFixed(3)}, Y :  ${DataCollection.pointList[index].y.toStringAsFixed(3)}"),
                  );
                }),
          ),
          ButtonBar(
            children: [
              RaisedButton.icon(
                onPressed: () {
                  if (DataCollection.pointList.length > 0) {
                    setState(() {
                      DataCollection.pointList.removeLast();
                    });
                  }
                },
                icon: Icon(Icons.undo),
                label: Text(
                  'UNDO',
                  style: TextStyle(fontSize: 20),
                ),
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                color: Colors.amber[900],
              )
            ],
          )
        ],
      ),
    );
  }
}

class JogBuild extends StatefulWidget {
  @override
  _JogBuildState createState() => _JogBuildState();
}

class _JogBuildState extends State<JogBuild> {
  bool _isJog = false;
  Timer _jogPostTimer;
  @override
  void dispose() {
    _jogPostTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.lightBlue, borderRadius: BorderRadius.circular(20)),
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Switch(
              value: _isJog,
              onChanged: (bool val) {
                setState(() {
                  _isJog = val;
                });
                if (val == true) {
                  _jogPostTimer =
                      Timer.periodic(Duration(milliseconds: 70), (timer) {
                    if (AgvServices.jogReady && _isJog) {
                      AgvServices.putJog(
                          DataCollection.axisX,
                          DataCollection.axisY,
                          DataCollection.speedValue.toStringAsFixed(3));
                    }
                  });
                } else {
                  _jogPostTimer?.cancel();
                }
              }),
          JoystickView(
            size: 180,
            interval: Duration(milliseconds: 50),
            onDirectionChanged: (theta, r) {
              DataCollection.axisX = theta.toStringAsFixed(3);
              DataCollection.axisY = r.toStringAsFixed(3);
              // print('X = ${DataCollection.axisX} Y = ${DataCollection.axisY}');
            },
          ),
        ],
      ),
    );
  }
}

class PathDetail extends StatefulWidget {
  @override
  _PathDetailState createState() => _PathDetailState();
}

class _PathDetailState extends State<PathDetail> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        width: 300,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              decoration: InputDecoration(
                labelText: "Origin",
              ),
              onChanged: (val) {
                DataCollection.pathOrigin = val;
              },
            ),
            TextField(
              decoration: InputDecoration(
                labelText: "Destination",
              ),
              onChanged: (val) {
                DataCollection.pathDestination = val;
              },
            )
          ],
        ),
      ),
    );
  }
}

class DataCollection {
  static double speedValue = 0.3;
  static double accelValue = 0.1;
  static double coordinateX = 0.0;
  static double coordinateY = 0.0;
  static var axisX = "0.0";
  static var axisY = "0.0";
  static var homePoint = PointData(0, 0, 0, 0.3, 0.1);
  static List<PointData> pointList = [];
  static String pathOrigin;
  static String pathDestination;
}
