// import 'dart:async';

import 'package:flutter/material.dart';
import 'package:agv_gui/path_teach_page.dart';
import 'package:agv_gui/api.dart';

double _iconSize = 40.0;

class ProgramPage extends StatefulWidget {
  @override
  _ProgramPageState createState() => _ProgramPageState();
}

class _ProgramPageState extends State<ProgramPage> {
  TextEditingController _jobNameController;
  @override
  void initState() {
    super.initState();
    _jobNameController = TextEditingController();
  }

  void dispose() {
    _jobNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AGV Path Program", style: TextStyle(color: Colors.white)),
      ),
      body: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              children: [
                ControlButtonBuild(),
                ProgramListBuild(),
                ButtonBar(
                  children: [
                    RaisedButton(
                      onPressed: () {
                        setState(() {
                          ProgramPageData.programTaskList = [];
                          ProgramPageData.programNameList = [];
                        });
                      },
                      child: Text("CLEAR"),
                    ),
                    RaisedButton(
                      onPressed: () {
                        if (ProgramPageData.programTaskList.isNotEmpty) {
                          AgvServices.programPost(
                              ProgramPageData.programNameList);
                        }
                      },
                      child: Text('EXECUTE'),
                    ),
                    RaisedButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text("Program detail"),
                            content: TextField(
                              controller: _jobNameController,
                            ),
                            actions: [
                              FlatButton(
                                  onPressed: () => Navigator.pop(context, "OK"),
                                  child: Text("OK"))
                            ],
                          ),
                        ).then((value) {
                          if (value == "OK") {
                            ProgramData data = ProgramData(
                                _jobNameController.text,
                                ProgramPageData.programNameList);
                            AgvServices.programSave(data);
                          }
                        });
                      },
                      child: Text("SAVE"),
                    )
                  ],
                )
              ],
            ),
            // NormalTaskBuild(),
            JobListBuild(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('ADD PATH'),
        icon: Icon(Icons.add),
        onPressed: () {
          MaterialPageRoute teachJobPageRoute =
              MaterialPageRoute(builder: (BuildContext context) => JobTeach());
          Navigator.of(context).push(teachJobPageRoute).then((value) {
            setState(() {});
          });
        },
      ),
    );
  }
}

class ControlButtonBuild extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          IconButton(
            icon: Icon(Icons.play_circle_fill),
            iconSize: _iconSize,
            onPressed: () {
              AgvServices.commandPut('start');
            },
            color: Colors.lightGreenAccent[700],
          ),
          IconButton(
            icon: Icon(Icons.pause_circle_filled),
            iconSize: _iconSize,
            onPressed: () {
              AgvServices.commandPut('stop');
            },
            color: Colors.amber,
          ),
          IconButton(
            icon: Icon(Icons.stop_circle_outlined),
            onPressed: () {},
            iconSize: _iconSize,
            color: Colors.red,
          ),
          IconButton(
            icon: Icon(Icons.loop),
            onPressed: () {},
            iconSize: _iconSize,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}

class ProgramListBuild extends StatefulWidget {
  @override
  _ProgramListBuildState createState() => _ProgramListBuildState();
}

class _ProgramListBuildState extends State<ProgramListBuild> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.lightBlue[100]),
      child: DragTarget(
        builder: (BuildContext context, List<int> candidateData,
            List<dynamic> rejectedData) {
          return SizedBox(
            width: 300,
            height: 400,
            child: ListView.builder(
              itemCount: ProgramPageData.programTaskList.length,
              itemBuilder: (listContext, listIndex) {
                return ListTile(
                  leading: CircleAvatar(
                    child: ProgramPageData.programTaskList[listIndex].icon,
                  ),
                  title: Text(ProgramPageData.programTaskList[listIndex].name),
                );
              },
            ),
          );
        },
        onWillAccept: (data) {
          return true;
        },
        onAccept: (i) {
          setState(() {
            if (i < ProgramPageData.normalTaskList.length) {
              ProgramPageData.programTaskList
                  .add(ProgramPageData.normalTaskList[i]);
              ProgramPageData.programNameList
                  .add(ProgramPageData.normalTaskList[i].name);
            } else {
              ProgramPageData.programTaskList.add(TaskData(
                  ProgramPageData.pathTaskList[i - 10].name,
                  Icon(Icons.location_city),
                  Colors.amber));
              ProgramPageData.programNameList
                  .add(ProgramPageData.pathTaskList[i - 10].name);
            }
          });
        },
      ),
    );
  }
}

class NormalTaskBuild extends StatefulWidget {
  @override
  _NormalTaskBuildState createState() => _NormalTaskBuildState();
}

class _NormalTaskBuildState extends State<NormalTaskBuild> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Text(
          'Common Task',
          style: TextStyle(color: Colors.brown[900], fontSize: 30),
        ),
        SizedBox(
          height: 400,
          width: 300,
          child: ListView.builder(
            itemCount: ProgramPageData.normalTaskList.length,
            itemBuilder: (listContext, listIndex) {
              return Draggable(
                child: ListTile(
                  leading: CircleAvatar(
                    child: ProgramPageData.normalTaskList[listIndex].icon,
                  ),
                  title: Text(ProgramPageData.normalTaskList[listIndex].name),
                  tileColor: ProgramPageData.normalTaskList[listIndex].color,
                ),
                feedback: CircleAvatar(
                  child: ProgramPageData.normalTaskList[listIndex].icon,
                ),
                data: listIndex,
              );
            },
          ),
        )
      ],
    ));
  }
}

class JobListBuild extends StatefulWidget {
  @override
  _JobListBuildState createState() => _JobListBuildState();
}

class _JobListBuildState extends State<JobListBuild> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Text(
          'Navigation Path',
          style: TextStyle(color: Colors.brown[900], fontSize: 30, height: 2),
        ),
        // SizedBox(
        //   height: 20,
        // ),
        FutureBuilder(
          future: AgvServices.pathGet(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<PathData> listPath = snapshot.data;
              ProgramPageData.pathTaskList = listPath;
              return SizedBox(
                height: 400,
                width: 300,
                child: ListView.builder(
                  itemCount: listPath.length,
                  itemBuilder: (listContext, listIndex) {
                    return Draggable(
                        data: listIndex + 10,
                        feedback: Icon(Icons.home),
                        child: ListTile(
                          leading: CircleAvatar(
                            child: Text(listIndex.toString()),
                            backgroundColor: Colors.blueAccent,
                          ),
                          tileColor: Colors.amber,
                          title: Text(listPath[listIndex].name),
                          trailing: IconButton(
                            color: Colors.red,
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              setState(() {
                                AgvServices.deletePath(
                                    listPath[listIndex].name);
                              });
                            },
                          ),
                        ));
                  },
                ),
              );
            }
            return CircularProgressIndicator();
          },
        ),
      ],
    ));
  }
}

class ProgramPageData {
  static List<TaskData> normalTaskList = [
    TaskData('HOME', Icon(Icons.home), Colors.green[100])
  ];
  static List<TaskData> programTaskList = [];
  static List<String> programNameList = [];
  static List<PathData> pathTaskList = [];
}
