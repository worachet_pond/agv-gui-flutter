import 'package:flutter/material.dart';
import 'package:agv_gui/program_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    var children2 = [
      Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              gradient: LinearGradient(
                  colors: [Colors.yellow[100], Colors.green[100]])),
          margin: EdgeInsets.all(32),
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              buildTextFieldEmail(),
              buildTextFieldPassword(),
              buildButtonSignIn(),
            ],
          )),
    ];
    return Scaffold(
        appBar: AppBar(
          title: Text("AGV Management", style: TextStyle(color: Colors.white)),
        ),
        body: Container(
            color: Colors.grey[800], child: Column(children: children2)));
  }

  Container buildButtonSignIn() {
    return Container(
      margin: EdgeInsets.only(top: 12),
      constraints: BoxConstraints.expand(height: 50),
      child: RaisedButton(
        child: Text("Sign in",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 18, color: Colors.purple[800])),
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.lightGreenAccent,
        onPressed: () {
          MaterialPageRoute programPageRoute = MaterialPageRoute(
              builder: (BuildContext context) => ProgramPage());
          Navigator.of(context).pushReplacement(programPageRoute);
        },
      ),
    );
  }

  Container buildTextFieldEmail() {
    return Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.yellow[50], borderRadius: BorderRadius.circular(16)),
        child: TextField(
            decoration: InputDecoration.collapsed(hintText: "User name"),
            style: TextStyle(fontSize: 18)));
  }

  Container buildTextFieldPassword() {
    return Container(
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.only(top: 12),
        decoration: BoxDecoration(
            color: Colors.yellow[50], borderRadius: BorderRadius.circular(16)),
        child: TextField(
            obscureText: true,
            decoration: InputDecoration.collapsed(hintText: "Password"),
            style: TextStyle(fontSize: 18)));
  }
}
