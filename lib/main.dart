import 'package:flutter/material.dart';
// import 'package:agv_gui/login_page.dart';
import 'package:agv_gui/dashboard.dart';
import 'package:flutter/services.dart';

void main() => runApp(LoginApp());

class LoginApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    return MaterialApp(
      title: 'AGV management system',
      home: DashBoardPage(),
    );
  }
}
