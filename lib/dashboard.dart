import 'package:flutter/material.dart';
import 'package:agv_gui/api.dart';
import 'package:agv_gui/program_page.dart';
import 'dart:async';
import 'dart:ui' as ui;

class DashBoardPage extends StatefulWidget {
  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  int _batteryPercent = 0;
  StreamController<int> batteryController = StreamController<int>();
  Timer _batteryGetTimer;
  @override
  void initState() {
    super.initState();
    batteryController.add(_batteryPercent);
    _batteryGetTimer = Timer.periodic(
        Duration(seconds: 10),
        (timer) => AgvServices.getBattery().then((double value) {
              int percentBatt = ((value - 25.6) * 8).round() * 10;
              if (percentBatt > 100) {
                percentBatt = 100;
              } else if (percentBatt < 0) {
                percentBatt = 0;
              }
              _batteryPercent = percentBatt;
              batteryController.add(_batteryPercent);
            }));
  }

  @override
  void dispose() {
    _batteryGetTimer?.cancel();
    batteryController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Row(
            children: [
              Text(
                "AGV Dashboard   ",
                style: TextStyle(fontSize: 20),
              ),
              Icon(
                Icons.battery_std_outlined,
                color: Colors.lightGreen,
              ),
              StreamBuilder(
                stream: batteryController.stream,
                builder: (context, snapshot) {
                  return Text("${snapshot.data.toString()}%");
                },
              ),
            ],
          ),
          actions: []),
      body: FutureBuilder(
        future: AgvServices.getProgram(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List programList = snapshot.data;
            return GridView.count(
              crossAxisCount: 4,
              children: List.generate(programList.length, (index) {
                ProgramData program = programList[index];
                return GestureDetector(
                  child: GridTile(
                    child: Card(
                        color: Colors.blue[200],
                        child: Center(
                          child: Text(
                            program.destination,
                            style: TextStyle(fontSize: 35),
                          ),
                        )),
                  ),
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("Confirm"),
                          content: SizedBox(
                            width: 600,
                            height: 400,
                            child: CustomPaint(
                              painter: PathDrawer([
                                Offset(0, 0),
                                Offset(100, 100),
                                Offset(100, 0)
                              ]),
                              size: Size(400, 400),
                            ),
                          ),
                          actions: <Widget>[
                            RaisedButton(
                              onPressed: () => Navigator.pop(context, "OK"),
                              child: Text(
                                "OK",
                                style: TextStyle(fontSize: 20),
                              ),
                              color: Colors.green,
                              padding: EdgeInsets.all(20),
                            ),
                            RaisedButton(
                              onPressed: () => Navigator.pop(context, "CANCEL"),
                              child: Text("Cancel",
                                  style: TextStyle(fontSize: 20)),
                              color: Colors.red,
                              padding: EdgeInsets.all(20),
                            )
                          ],
                        );
                      },
                    ).then((value) {
                      if (value == "OK") {
                        AgvServices.programPost(program.path);
                        showDialog(
                            context: context,
                            builder: (context) {
                              return RunningProgress();
                            });
                      }
                    });
                  },
                );
              }),
            );
          }
          return CircularProgressIndicator();
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          MaterialPageRoute programPageRoute = MaterialPageRoute(
              builder: (BuildContext context) => ProgramPage());
          Navigator.of(context).push(programPageRoute).then((value) {
            setState(() {});
          });
        },
      ),
    );
  }
}

class PathDrawer extends CustomPainter {
  List<Offset> p;
  PathDrawer(List<Offset> _p) {
    p = _p;
  }
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.blue
      ..strokeWidth = 4;
    canvas.drawPoints(ui.PointMode.polygon, this.p, paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}

class MyPainter extends CustomPainter {
  //         <-- CustomPainter class
  @override
  void paint(Canvas canvas, Size size) {
    //                                             <-- Insert your painting code here.
    final pointMode = ui.PointMode.polygon;
    final points = [
      Offset(200, 400),
      Offset(200, 300),
      Offset(100, 300),
      Offset(100, 100),
      Offset(270, 100),
    ];
    final paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round;
    canvas.drawPoints(pointMode, points, paint);
    paint.color = Colors.blue;
    canvas.drawCircle(points[4], 10, paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}

class RunningProgress extends StatefulWidget {
  @override
  _RunningProgressState createState() => _RunningProgressState();
}

class _RunningProgressState extends State<RunningProgress> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("AGV on progress"),
    );
  }
}
